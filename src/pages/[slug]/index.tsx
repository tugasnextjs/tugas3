import { useState, useEffect } from 'react'
import Link from 'next/link'
import type { InferGetStaticPropsType } from 'next'

type Article = {
  name: string
}
function DetailPage ({article,}: InferGetStaticPropsType<typeof getStaticProps>) {
  const [article1] = useState(article.data)
  const [articlemore, setArticlemore] = useState<any[]>([])
  
  useEffect(() => {
    //if(article.length>0){
      fetch('https://hsi-sandbox.vercel.app/api/articles?perPage=2&excludedArticleId='+article1.id+'&categoryId='+article1.category.id)
      .then((res) => res.json())
      .then((data) => {
        setArticlemore(data.data)
      })
    //} 
  }, [article])

    return(
      <div>
        <img className='center' src="logo.png" alt="logo"/><br/>
        <div className='left'>
          <h1>{article1.title}</h1><br/>
          <b>{article1.summary}</b><br/><br/>
          <p className="upperCase"><span  className="gray">BY</span> {article1.author.firstName} {article1.author.middleName} {article1.author.lastName} <span  className="gray">IN</span> {article1.category.name}</p>
          <br/><img className='imageDetail' src={article1.thumbnail}/><br/><br/>
          <p className="gray">{article1.content}</p>
        </div>
        <div className='left'>
          <div className="column">
            <h1>You might also like...</h1>
          </div>
          <div className="columnRight">
          <Link href={'/'+article1.slug+'/relates'}><p className="gray"> More</p></Link>
          </div>
        </div><br/><br/>
        <div className='left'>
        {articlemore.map((articleNew) => (
            <div className='column' key={articleNew.id}>
              <img className='imageArticleSmall' src={articleNew.thumbnail}/><br/><br/>
              <p className="upperCase"><span  className="gray">BY</span> {articleNew.author.firstName} {articleNew.author.middleName} {articleNew.author.lastName} <span  className="gray">IN</span> {articleNew.category.name}</p><br/>
              <Link href={'/'+articleNew.slug}><h1> {articleNew.title}</h1></Link><br/>
              <p className="gray">{articleNew.summary}</p>
            </div>
          ))}
        </div>
      </div>
    )
  }


  export const getStaticProps = async (context:any) => {
    const slug = context.params.slug;
    const res = await fetch('https://hsi-sandbox.vercel.app/api/articles/'+slug);
    const article = await res.json()
    return { props: { article} }
  }

  export const getStaticPaths = async () => {
    const res1 = await fetch('https://hsi-sandbox.vercel.app/api/articles');
    const data1= await res1.json();
    const totalPages = data1.meta.pagination.totalPages;
    const collectionData = data1.data;
    if(totalPages>1){
      for(let i=2;i<=totalPages;i++){
        const res2 = await fetch('https://hsi-sandbox.vercel.app/api/articles?page='+i);
        const data2= await res2.json();
        collectionData.push(...data2.data);
      }
    }
    const paths = collectionData.map((article:any) =>{
      return {
        params: {
          slug: article.slug
        }
      }
    })
    return {
      paths,
      fallback: false, // false or "blocking"
    }
  }
  
export default DetailPage;