import Link from 'next/link'
import { useState, useEffect, use } from 'react'

function RelatesPage ({data1, data2}:any) {
    const article = data1.data;
    const [related, setRelated] = useState(data2.data);
    const [pagenew, setPagenew] = useState(1);
    const [totalnew] = useState(data2.meta.pagination.totalPages);

    useEffect(() => {
        const element = document.getElementById("buttonLoadNew");
        if(pagenew>1){
            
            if(element){
                if(pagenew==totalnew){
                    element.style.display = "none";
                  }else{
                    element.style.display = "block";
                  }
            }
          fetch('https://hsi-sandbox.vercel.app/api/articles?excludedArticleId='+data1.data.id+'&categoryId='+data1.data.category.id+'&page='+pagenew)
          .then((res) => res.json())
          .then((data) => {
            setRelated([...related,...data.data])
            console.log(data.data);
          })
        }
      }, [pagenew])
    
    useEffect(() => {
        const element = document.getElementById("buttonLoadNew");
        if(element){
            if(totalnew==1){
                element.style.display = "none";
            }else{
                element.style.display = "block";
            }
        }
    }, [totalnew])

    return(
        <div>
            <div className="bgwhite" style={{height: "400px"}}>
            <img className='center' src="/logo.png" alt="logo"/><br/><br/><br/><br/><br/><br/>
                <div style={{width: "1168px"}}>
                <h1>Related Post List</h1><br/>
                    <div className='imageRelatedAtas'>
                        <img src={article.thumbnail} style={{width: "230px"}}/><br/><br/>
                    </div>
                    <div className="columnRelatedAtas">
                        <Link href={'/'+article.slug}><h1> {article.title}</h1></Link><br/>
                        <b>{article.summary}</b>
                    </div>
                </div>
            </div>
            <div className='bggrey'>
            {related.map((relatedArticle:any,index:any) => (
                <div className="bgwhite" style={{width: "1168px", height: "358px"}} key={relatedArticle.id}>
                    <div>
                        <div className='columnRelatedBawah'>
                            <h1>{(index+1).toString().padStart(2, '0')}</h1><br/>
                            <Link href={'/'+relatedArticle.slug}><h1> {relatedArticle.title}</h1></Link><br/>
                            <p className='gray'>{relatedArticle.summary}</p>
                        </div>
                        <div className='imageRelatedBawah'>
                            <img className='imageRelatedBawah2' src={relatedArticle.thumbnail}/>
                        </div>
                    </div>
                </div>
            ))}
            </div>
            <div className='center'>
                <button id='buttonLoadNew' className='buttonLoad' onClick={() => setPagenew((c) => c + 1)}>Load More</button>
            </div>
        </div>
    )
}

export const getServerSideProps = (async (context:any) => {
    const slug = context.params.slug;
    const res1 = await fetch('https://hsi-sandbox.vercel.app/api/articles/'+slug);
    const data1 = await res1.json();
    
    if(data1.message){
        return {
            notFound: true,
        }
    }
    const res2 = await fetch('https://hsi-sandbox.vercel.app/api/articles?excludedArticleId='+data1.data.id+'&categoryId='+data1.data.category.id);
    const data2 = await res2.json();
    /**
     */
    
    return { props: { data1, data2 } }
  })

export default RelatesPage;