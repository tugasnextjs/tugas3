import { useRouter } from 'next/router'
import { useState, useEffect } from 'react'
import Link from 'next/link'
function Home ({dataNew1, dataPop1}:any){
  const router = useRouter();
  const [pagenew, setPagenew] = useState(1);
  const [pagepop, setPagepop] = useState(1);
  const [totalnew] = useState(dataNew1.meta.pagination.totalPages);
  const [articlenew, setArticlenew] = useState(dataNew1.data);
  const [articlepop, setArticlepop] = useState(dataPop1.data);

  const updateColorButtonHeader = (value: string) => {
    const elementnew = document.getElementById("new");
    const elementpopular = document.getElementById("popular");
    const elementnewtab = document.getElementById("newtab");
    const elementpopulartab = document.getElementById("populartab");
    if(elementnew&&elementpopular&&elementnewtab&&elementpopulartab){
      if(value=="popular"){
        elementpopular.style.backgroundColor = "#FF5480";
        elementnew.style.backgroundColor = "transparent";
        elementpopular.style.color = "white";
        elementnew.style.color = "black";
        elementpopulartab.style.display = "block";
        elementnewtab.style.display = "none";
      }else{
        elementnew.style.backgroundColor = "#FF5480";
        elementpopular.style.backgroundColor = "transparent";
        elementnew.style.color = "white";
        elementpopular.style.color = "black";
        elementpopulartab.style.display = "none";
        elementnewtab.style.display = "block";
      }
    }
    
  };

  

  useEffect(() => {
    if(pagenew>1){
      console.log("Halaman new sekarang: "+ pagenew);
      const elementLoadNew = document.getElementById("buttonLoadNew");
      if(elementLoadNew){
        if(pagenew==totalnew){
          elementLoadNew.style.display = "none";
        }else{
          elementLoadNew.style.display = "block";
        }
      }
      
      
      fetch('https://hsi-sandbox.vercel.app/api/articles?page='+pagenew)
      .then((res) => res.json())
      .then((data) => {
        setArticlenew([...articlenew,...data.data])
        console.log(data.data);
      })
    }
  }, [pagenew])

  useEffect(() => {
    if(pagepop>1){
      const elementLoadPop = document.getElementById("buttonLoadPop");
      if(elementLoadPop){
        console.log("Halaman popular sekarang: "+ pagepop+", Halaman total: "+totalnew);
        if(pagepop==totalnew){
          elementLoadPop.style.display = "none";
        }else{
          elementLoadPop.style.display = "block";
        }
      }
      fetch('https://hsi-sandbox.vercel.app/api/articles?sort=popular&page='+pagepop)
      .then((res) => res.json())
      .then((data) => {
        setArticlepop([...articlepop,...data.data])
      })
    }
  }, [pagepop])

  return (
    <div>
      <button id="popular" className='buttonHeader' onClick={()=>{updateColorButtonHeader("popular");router.push("/?sort=popular")}}>Popular</button>
      <button id="new" className='buttonHeader' onClick={()=>{updateColorButtonHeader("new");router.push("/?sort=new")}}>New</button>
      <img className='center' src="logo.png" alt="logo"/>
      <div  id="newtab">
        <div className = "columnNew">
          {articlenew.map((articleNew:any) => (
            <div className='rowArticle' key={articleNew.id}>
              <img className='imageArticle' src={articleNew.thumbnail}/>
              <p className="upperCase"><span  className="gray">BY</span> {articleNew.author.firstName} {articleNew.author.middleName} {articleNew.author.lastName} <span  className="gray">IN</span> {articleNew.category.name}</p>
              <Link href={'/'+articleNew.slug}><h1> {articleNew.title}</h1></Link>
              </div>
          ))}
        </div>
        <div className='center'>
            <button id='buttonLoadNew' className='buttonLoad' onClick={() => setPagenew((c) => c + 1)}>Load More</button>
        </div>
      </div>
      <div  id="populartab" style={{display:"none"}}>
      <div className = "columnNew">
          {articlepop.map((articleNew:any) => (
            <div className='rowArticle' key={articleNew.id}>
              <img className='imageArticle' src={articleNew.thumbnail}/>
              <p className="upperCase"><span  className="gray">BY</span> {articleNew.author.firstName} {articleNew.author.middleName} {articleNew.author.lastName} <span  className="gray">IN</span> {articleNew.category.name}</p>
              <Link href={'/'+articleNew.slug}><h1> {articleNew.title}</h1></Link>
            </div>
          ))}
        </div>
        <div className='center'>
            <button id='buttonLoadPop' className='buttonLoad' onClick={() => setPagepop((c) => c + 1)}>Load More</button>
        </div>
      </div>
    </div>
  )
};

export const getServerSideProps = (async () => {
  const [resnew,respop] = await Promise.all([
    fetch('https://hsi-sandbox.vercel.app/api/articles'),
    fetch('https://hsi-sandbox.vercel.app/api/articles?sort=popular') 
  ]);
  const [dataNew1, dataPop1] = await Promise.all([
    resnew.json(), 
    respop.json()
  ]);
  return { props: { dataNew1, dataPop1 } }
})



export default Home;


